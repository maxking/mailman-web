import string
import random

from .base import *
from .mailman import *


def gen_random():
    """Generate a random secret key for dev workflow."""
    return ''.join(random.choices(string.ascii_uppercase +
                             string.digits, k=20))

SECRET_KEY = gen_random()
DEBUG = True

# Needed for debug mode
INTERNAL_IPS = ('127.0.0.1',)

# Allowed_hosts.
ALLOWED_HOSTS = [
    '127.0.0.1',
    'localhost',
    '::1',
    ]

# Use console backend as default so all the sent out emails
# are printed to stdout.
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"